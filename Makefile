SIZES = 16 22 24 32 48 256
NAMES = $(patsubst menu-icons/scalable/%.svg,%,$(wildcard menu-icons/scalable/*/*.svg))

all: polkit-1/actions/org.kali.pkexec.policy
	@$(MAKE) $(TARGETS)

polkit-1/actions/org.kali.pkexec.policy: x11-apps bin/generate-polkit
	./bin/generate-polkit >polkit-1/actions/org.kali.pkexec.policy

install:

clean:

# Add dynamic rules to convert all SVG files into various PNG
define svg_to_png
menu-icons/$(1)x$(1)/$(2).png: menu-icons/scalable/$(2).svg
	mkdir -p $(dir menu-icons/$(1)x$(1)/$(2).png)
	rsvg-convert -o $$@ -w $(1) -h $(1) $$<

TARGETS += menu-icons/$(1)x$(1)/$(2).png

endef

$(foreach SIZE,$(SIZES),$(foreach NAME,$(NAMES),$(eval $(call svg_to_png,$(SIZE),$(NAME))))) 
